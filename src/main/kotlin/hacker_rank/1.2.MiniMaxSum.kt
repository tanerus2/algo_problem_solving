// https://www.hackerrank.com/challenges/one-month-preparation-kit-mini-max-sum/problem?filter=kotlin&filter_on=language&h_l=interview&h_r=next-challenge&h_v=zen&isFullScreen=true&page=1&playlist_slugs[]=preparation-kits&playlist_slugs[]=one-month-preparation-kit&playlist_slugs[]=one-month-week-one

fun main() {
    // input:
    val arr: Array<Int> = arrayOf(256741038, 623958417, 467905213, 714532089, 938071625)

    // solution 1:
    val minSumOfFour = arr.sortedArray()
        .copyOfRange(0, 4)
        .map { it.toLong() }
        .sum()
    val maxSumOfFour = arr.sortedArrayDescending()
        .copyOfRange(0, 4)
        .map { it.toLong() }
        .sum()
    println("$minSumOfFour $maxSumOfFour")

    println("$minSumOfFour\n$maxSumOfFour")

    // solution 2 (sorting is made only once):
    solution2(arr)
}

fun solution2(arr: Array<Int>) {
    val sortedArray = arr.sortedArray()
    val minSumOfFour = sortedArray
        .copyOfRange(0, 4)
        .map { it.toLong() }
        .sum()
    val maxSumOfFour = sortedArray.reversedArray()
        .copyOfRange(0, 4)
        .map { it.toLong() }
        .sum()
    println("$minSumOfFour $maxSumOfFour")
}