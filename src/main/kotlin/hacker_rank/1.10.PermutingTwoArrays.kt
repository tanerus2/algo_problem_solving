// https://www.hackerrank.com/challenges/one-month-preparation-kit-two-arrays/problem?h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D=one-month-week-one

/*
 * Complete the 'twoArrays' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts following parameters:
 *  1. INTEGER k
 *  2. INTEGER_ARRAY A
 *  3. INTEGER_ARRAY B
 */

/*
1. find the least num in A
2. for nums in be starting from the least find the sum of two that is >= k (so A and B should be sorted)
3. exclude those indices from arrays (maybe -1)
4. repeat for each num (number of repetition will be size of and array)
5. if algorithm can't find such combination - return NO
 */

/* if we iterated through the sorted B array and didn't find sum like A[i] + B[j] >= k
 -> there is no combination for at least one num in A (so one pair is missed)
 -> answer is NO
 */

// works with i != j
fun twoArrays(k: Int, A: Array<Int>, B: Array<Int>): String {
    A.sort(); B.sort()
    var i = 0; var j = 0
    while (i != A.size) {
        if (j == B.size) {
            return "NO"
        } else if (A[i] + B[j] >= k) {
            A[i] = 0; B[j] = 0
            j = 0; i++
            continue
        }
        j++
    }
    return "YES"
}

// works only if i == j
fun twoArrays2(k: Int, A: Array<Int>, B: Array<Int>): String {
    A.sort(); B.sortDescending()
    A.forEachIndexed { index, num -> if (A[index] + B[index] < k) return "NO" }
    return "YES"
}


fun main() {
    val k = 3
    val A = arrayOf(2, 1, 3)
    val B = arrayOf(7, 8, 9)

    val k2 = 5
    val A2 = arrayOf(1, 2, 2, 1)
    val B2 = arrayOf(3, 3, 3, 4)

    println(twoArrays(k, A, B))
    println(twoArrays(k2, A2, B2))

    println(twoArrays2(k, A, B))
    println(twoArrays2(k2, A2, B2))
}