// https://www.hackerrank.com/challenges/one-month-preparation-kit-sparse-arrays/problem?h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D=one-month-week-one&h_r=next-challenge&h_v=zen

/*
 * Complete the 'matchingStrings' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. STRING_ARRAY strings
 *  2. STRING_ARRAY queries
 */

fun matchingStrings2(strings: Array<String>, queries: Array<String>): Array<Int> {
    val stringsCount = strings.groupBy { it }.mapValues { map -> map.value.size }
    return queries.map { stringsCount.getOrDefault(it, 0) }.toTypedArray()
}

fun matchingStrings(strings: Array<String>, queries: Array<String>): Array<Int>  {
    val resultList = mutableListOf<Int>()
    val cache = mutableMapOf<String, Int>()
    var counter = 0
    for (i in 0..(queries.size - 1)) {
        if (i != 0 && cache.containsKey(queries[i])) {
            resultList.add(cache.getValue(queries[i]))
        } else {
            strings.forEach { if (it == queries[i]) counter++ }
            resultList.add(counter)
            cache.put(queries[i], counter)
        }
        counter = 0
    }
    return resultList.toTypedArray()
}

fun main() {
    // input:
    val strs1 = arrayOf("aba", "baba", "aba", "xzxb")
    val qrs1 = arrayOf("aba", "xzxb", "ab")

    val strs2 = arrayOf("def", "de", "fgh")
    val qrs2 = arrayOf("de", "lmn", "fgh")

    // expected output:
    val exp1 = arrayOf(2, 1, 0)
    val exp2 = arrayOf(1, 0, 1)

    // result:
    var result = matchingStrings(strs1, qrs1)
    if (exp1.contentEquals(result)) {
        println("Yes")
    } else {
        println("1 - No")
        println(result.toList())
    }

    result = matchingStrings(strs2, qrs2)
    if (exp2.contentEquals(result)) {
        println("Yes")
    } else {
        println("2 - No")
        println(result.toList())
    }
}