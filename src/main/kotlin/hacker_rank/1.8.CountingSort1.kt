// https://www.hackerrank.com/challenges/one-month-preparation-kit-countingsort1/problem?h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D=one-month-week-one

/*
 * Complete the 'countingSort' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts INTEGER_ARRAY arr as parameter.
 */
fun countingSort(arr: Array<Int>): Array<Int> {
    val countingArray = IntArray(100)
    arr.forEach { countingArray[it]++ }
    return countingArray.toTypedArray()
}
