// https://www.hackerrank.com/challenges/one-month-preparation-kit-pangrams/problem?h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D=one-month-week-one

/*
 * Complete the 'pangrams' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts STRING s as parameter.
 */
fun pangrams(s: String) = if (s.lowercase().filterNot { it == ' ' }.count() == 26) "pangram" else "not pangram"

fun main() {
    println(pangrams("We promptly judged antique ivory buckles for the prize"))
}