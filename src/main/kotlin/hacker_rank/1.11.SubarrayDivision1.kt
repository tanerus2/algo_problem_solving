package hacker_rank

// https://www.hackerrank.com/challenges/one-month-preparation-kit-the-birthday-bar/problem?h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D=one-month-week-one

/*
 * Complete the 'birthday' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY s
 *  2. INTEGER d
 *  3. INTEGER m
 */

/* 1 <= arraySize <= 100
 * 1 <= each integer <= 5
 * 1 <= day <= 31
 * 1 <= month <= 12
 *
 */
fun birthday(s: Array<Int>, d: Int, m: Int): Int {
    if (s.size == m)
        return if (s.sum() == d) 1 else 0
    var possibilities = 0
    var subArray: Array<Int>
    for (i in 0 until (s.size - m)) {
        subArray = s.copyOfRange(i, i + m)
        if (subArray.sum() == d) possibilities++
    }
    if (s.copyOfRange(s.size - m, s.size).sum() == d)
        possibilities++
    return possibilities
}

fun birthday2(s: Array<Int>, d: Int, m: Int): Int {
    if (s.size == m) return if (s.sum() == d) 1 else 0
    return s.take(s.size - m + 1)
           .filterIndexed { index, _ -> s.copyOfRange(index, index + m).sum() == d }
           .count()
}

fun main() {
    val chocoBar = arrayOf(2, 5, 1, 3, 4, 4, 3, 5, 1, 1, 2, 1, 4, 1, 3, 3, 4, 2, 1)
    val day = 18
    val month = 7

    println(3 == birthday2(chocoBar, day, month))
    println(birthday2(chocoBar, day, month))
}