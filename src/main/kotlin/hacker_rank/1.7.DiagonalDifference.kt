import kotlin.math.absoluteValue

// https://www.hackerrank.com/challenges/one-month-preparation-kit-diagonal-difference/problem?h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D=one-month-week-one&h_r=next-challenge&h_v=zen

/*
 * Complete the 'diagonalDifference' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts 2D_INTEGER_ARRAY arr as parameter.
 */
fun diagonalDifference(arr: Array<Array<Int>>): Int {
    val size = arr.size
    var difference = 0
    for (i in 0 until size) {
        difference += arr[i][i]
        difference -= arr[i][(size - i) - 1]
    }
    return Math.abs(difference)
}

fun main() {
    val testArray: Array<Array<Int>> = arrayOf(arrayOf(11, 2, 4),
                                               arrayOf(4, 5, 6),
                                               arrayOf(10, 8, -12))
    println(diagonalDifference(testArray))
}