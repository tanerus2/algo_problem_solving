// https://www.hackerrank.com/challenges/one-month-preparation-kit-plus-minus/problem?h_l=interview&playlist_slugs[]=preparation-kits&playlist_slugs[]=one-month-preparation-kit&playlist_slugs[]=one-month-week-one

fun main() {
    // input:
    val arr: Array<Int> = arrayOf(1,2,3, 0, -1, 0 ,1, 2, 3, -2)

    // solution:
    val pairOfPosAndNeg: Pair<List<Int>, List<Int>> = arr.filter { it != 0 }.partition { it > 0 }
    val numOfPosAndNeg = pairOfPosAndNeg.first.size + pairOfPosAndNeg.second.size

    val ratioOfZeroes: Double = (arr.size - numOfPosAndNeg).toDouble() / arr.size
    val ratioOfPos = pairOfPosAndNeg.first.size / arr.size.toDouble()
    val ratioOfNeg = pairOfPosAndNeg.second.size / arr.size.toDouble()

    println(String.format("%s\n%s\n%s", ratioOfPos, ratioOfNeg, ratioOfZeroes))
}