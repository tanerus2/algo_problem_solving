// https://www.hackerrank.com/challenges/one-month-preparation-kit-lonely-integer/problem?h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D=one-month-week-one&h_r=next-challenge&h_v=zen

fun lonelyInteger(a: Array<Int>): Int {
    return a.groupBy { it }
        .mapValues { map -> map.value.size }
        .filterValues { it == 1 }
        .map { map -> map.key }
        .get(0)
}

fun main() {
    println(lonelyInteger(arrayOf(1,2,3,4,3,2,1)))
}