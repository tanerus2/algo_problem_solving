// https://www.hackerrank.com/challenges/one-month-preparation-kit-mini-max-sum/problem?filter=kotlin&filter_on=language&h_l=interview&h_r=next-challenge&h_v=zen&isFullScreen=true&page=1&playlist_slugs[]=preparation-kits&playlist_slugs[]=one-month-preparation-kit&playlist_slugs[]=one-month-week-one

/*
    12:00:00AM - 00:00:00
    12:59:59AM - 00:59:59
    01:00:00AM - 01:00:00
    11:59:59AM - 11:59:59

    12:00:00PM - 12:00:00
    12:59:59PM - 12:59:59
    01:00:00PM - 13:00:00
    11:59:59PM - 23:59:59

    if AM {
        if HH == 12 -> HH = 00
    } else {
        HH = HH
    }

    if PM {
        if PM == 12 -> HH == HH
    } else {
        HH = HH + 12
    }
*/

fun timeConversion(s: String): String {
    var militaryTime = s.substring(0, 8)
    val hours = s.substring(0, 2)
    val minAndSec = militaryTime.substring(2)
    val timeIndicator = s.substring(8)
    when (timeIndicator) {
        "AM" -> if (hours == "12") militaryTime = "00" + minAndSec
        "PM" -> if (hours != "12") {
            militaryTime = (hours.toInt() + 12).toString() + minAndSec
        }
    }
    return militaryTime
}

fun timeConversionVer2(s: String): String {
    var militaryTime = s.substring(0, 8)
    val timeIndicator = s.substring(8)
    when (timeIndicator) {
        "AM" -> militaryTime = if (militaryTime.substring(0, 2) == "12") "00" + militaryTime.substring(2) else militaryTime
        "PM" -> militaryTime = if (militaryTime.substring(0, 2) != "12") (militaryTime.substring(0, 2).toInt() + 12).toString() + militaryTime.substring(2) else militaryTime
    }
    return militaryTime
}

fun main() {
    // input:
    val time = "01:00:00AM"

    // output:
    println(timeConversion(time))
    println(timeConversionVer2(time))
}

