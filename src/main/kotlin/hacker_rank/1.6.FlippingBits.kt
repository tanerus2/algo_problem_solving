// https://www.hackerrank.com/challenges/one-month-preparation-kit-flipping-bits/problem?h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D=one-month-week-one

/*
    To binary:
   57 / 2 = 28 and 1/2
   28 / 2 = 14 and 0/2
   14 / 2 = 7 and 0/2
   7 / 2 = 3 and 1/2
   3 / 2 = 1 and 1/2
   1 --> 1/2

   57 == 111001
    */

fun main() {
    val num =4294950911L
    println("Number: $num")

    println("Binary:\n" + num.toBinary().toList())

    val arrayOfBits = num.toBinary()
    val numFromBits = arrayOfBits.toDecimal()

    println("Number: $numFromBits")


    val n = 4L
    val result = flippingBits(n)
    println(result)
}

/*
 * Complete the 'flippingBits' function below.
 *
 * The function is expected to return a LONG_INTEGER.
 * The function accepts LONG_INTEGER n as parameter.
 */

fun flippingBits(n: Long): Long {
    val bits = IntArray(32)
    n.toBinary().withIndex().forEach { (index, bit) ->  bits[index] = if (bit == 1) 0 else 1 }
    return bits.toDecimal()
}

fun Long.toBinary(): IntArray {
    var number = this
    val bits = IntArray(32)
    var index = 0
    while (number >= 2) {
        bits[index] = (number % 2).toInt()
        number /= 2
        index++
    }
    bits[index] = (number % 2).toInt()
    return bits.reversedArray()
}

fun IntArray.toDecimal(): Long {
    var decimalNum = 0L
    var base = 31.0
    for (bit in this) {
        decimalNum += bit * Math.pow(2.0, base).toLong()
        base--
    }
    return decimalNum
}
