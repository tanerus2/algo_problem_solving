package code_wars

fun areaOfPerimeter(l: Int, w: Int): Int = if (l == w) l * l else 2 * l + 2 * w

fun main() {
    println(areaOfPerimeter(6, 10))
}