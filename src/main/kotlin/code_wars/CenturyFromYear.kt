package code_wars

import kotlin.test.assertEquals

fun main() {
    assertEquals(18, century(1705));
    assertEquals(19, century(1900));
    assertEquals(17, century(1601));
    assertEquals(20, century(2000));
    assertEquals(1,  century(89));
}

fun century(number: Int): Int {
    var century = number
    if (number % 1000 != number) {
        century /= 100
        return if (century % 10 != 0) century.inc() else century
    }
    century /= 10
    return if (century % 10 != 0) century / 10 + 1 else century/10
}