package code_wars

// https://www.codewars.com/kata/56b5afb4ed1f6d5fb0000991

fun revRot(nums: String, sz: Int): String {
    if (sz <= 0 || nums.isEmpty() || sz > nums.length)
        return ""
    val numOfChunks = nums.length / sz
    var chunk: String
    var index = 0
    val chunks: Array<String> = Array(numOfChunks) {
        chunk = nums.substring(index, index + sz)
        index += sz
        var sumOfQubes = 0
        var digit: Int
        for (char in chunk) {
            digit = char.digitToInt()
            sumOfQubes += digit * digit * digit
        }
        if (sumOfQubes % 2 == 0) {
            chunk.reversed()
        } else {
            chunk.substring(1, chunk.length) + chunk.substring(0, 1)
        }
    }
    return chunks.joinToString("")
}

fun revRot2(nums: String, sz: Int) =
    when {
        sz <= 0 || sz > nums.length -> ""
        else -> nums.chunked(sz).map {
            if (it.length < sz)
                ""
            else if (it.map { if (it.digitToInt() % 2 != 0) 1 else 0 }.sum() % 2 == 0) // number of odds
                it.reversed()
            else it.drop(1) + it[0]
        }.joinToString("")
    }

fun main() {
    println(revRot("733049910872815764", 5)) // 330479108928157
    println(330479108928157)
    val s = "abcdefghi"
    println(s.replace(Regex("[aeiouABCDE]"), "!"))

}