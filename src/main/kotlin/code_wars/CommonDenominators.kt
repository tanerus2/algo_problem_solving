package code_wars

// https://www.codewars.com/kata/54d7660d2daf68c619000d95

import kotlin.math.pow
import kotlin.math.sqrt

typealias FactorFrequencies = Map<Long, Int>

private const val FRACTION_MASK: String = "(%d,%d)"

data class Fraction(val numerator: Long, val denominator: Long)

/**
 * Converts fractions according to the least common denominator
 * Returns result String of converted fractions
 */
fun convertFrac(fractions: Array<Fraction>): String {
    if (fractions.isEmpty())
        return ""
    val fractionsReduced: Array<Fraction> = reduceFractions(fractions)
    val denominators = fractionsReduced.map { it.denominator }
    if (denominators.all { it == 1L })
        return fractions.joinToString("") { String.format(FRACTION_MASK, it.numerator, it.denominator) }
    val denomPrimeFactors: Array<FactorFrequencies> = Array(denominators.size) {
        (getPrimeFactors(denominators[it]))
    }
    val leastCommonDenominator =
        getByFrequency(denomPrimeFactors, Math::max).map { it.key.toDouble().pow(it.value.toDouble()) }
            .reduce { acc, next -> acc * next }.toLong()
    return fractionsReduced.joinToString {
        val coefficient = leastCommonDenominator / it.denominator
        String.format(FRACTION_MASK, it.numerator * coefficient, leastCommonDenominator)
    }
}

fun reduceFractions(fractions: Array<Fraction>): Array<Fraction> {
    val reducedFractions = fractions.copyOf()
    var index = 0
    for ((numerator, denominator) in fractions) {
        if (denominator == 1L) continue
        val numerPrimeFactors: FactorFrequencies = getPrimeFactors(numerator)
        val denomPrimeFactors: FactorFrequencies = getPrimeFactors(denominator)
        val commonFactors: Set<Long> = numerPrimeFactors.keys.intersect(denomPrimeFactors.keys)
        if (commonFactors.isNotEmpty()) {
            val lessFrequentFactors =
                getByFrequency(arrayOf(numerPrimeFactors, denomPrimeFactors), Math::min)
            val coefficient = calcCoefficientOfReduction(lessFrequentFactors, commonFactors)
            reducedFractions[index] = Fraction(numerator / coefficient, denominator / coefficient)
        }
        index++
    }
    return reducedFractions
}

fun calcCoefficientOfReduction(lessFrequentFactors: FactorFrequencies, commonFactors: Set<Long>): Long {
    return commonFactors.map { commonFactor ->
        val frequency = lessFrequentFactors.getOrDefault(commonFactor, 1).toDouble()
        commonFactor.toDouble().pow(frequency)
    }.reduce { acc, next -> acc * next }.toLong()
}

/**
 * Returns Map of the most frequent prime factors ('factor to frequency' Map)
 */
fun getByFrequency(primeFactors: Array<FactorFrequencies>, op: (Int, Int) -> Int): FactorFrequencies {
    val resFactors = mutableMapOf<Long, Int>()
    primeFactors.flatMap { it.entries }
        .forEach() { resFactors[it.key] = op(it.value, resFactors.getOrPut(it.key) { it.value }) }
    return resFactors
}

/**
 * Finds all prime factors of the number
 * Returns a 'prime factor to frequency' Map
 */
fun getPrimeFactors(number: Long): FactorFrequencies {
    val sqrt = sqrt(number.toDouble())
    var currentValue = number
    var multiplier = 2L
    val listOfPrimeFactors = mutableListOf<Long>()
    while (currentValue > 1 && multiplier <= sqrt) {
        if (currentValue % multiplier == 0L) {
            listOfPrimeFactors.add(multiplier)
            currentValue /= multiplier
        } else if (multiplier == 2L) {
            multiplier++
        } else {
            multiplier += 2
        }
    }
    if (currentValue != 1L) {
        listOfPrimeFactors.add(currentValue)
    }
    return listOfPrimeFactors.groupBy { it }.mapValues { it.value.count() }
}

fun main() {
    val fractions = arrayOf(Fraction(69, 130), Fraction(87, 1310), Fraction(30, 40))
    println(convertFrac(fractions))
}