package code_wars

import kotlin.math.ceil
import kotlin.math.roundToInt

// https://www.codewars.com/kata/563b662a59afc2b5120000c6

fun nbYear(pp0: Int, percent: Double, aug: Int, p: Int): Int {
    var population = pp0
    var years = 0
    while (population < p) {
        population += (population * (percent / 100) + aug).roundToInt()
        years++
    }
    return years
}

fun nbYear2(pp0: Int, percent: Double, aug: Int, p: Int): Int =
    generateSequence(pp0.toDouble()) { it *  (1 + percent / 100) + aug }.takeWhile { it < p }.count()

fun main() {
    println(nbYear(1000, 2.0, 50, 1200))
}