package code_wars


fun deleteNth(elements: IntArray, maxOccurrences: Int): IntArray {
    val mapOfOccurrences = mutableMapOf<Int, Int>()
    val result = mutableListOf<Int>()
    for (elem in elements) {
        val occurrences = mapOfOccurrences.getOrPut(elem) { 0 }
        if (occurrences < maxOccurrences) {
            result.add(elem)
            mapOfOccurrences[elem] = occurrences.inc()
        }
    }
    return result.toIntArray()
}

fun deleteNth2(elements: IntArray, maxOccurrences: Int): IntArray {
    val mapOfOccurrences = mutableMapOf<Int, Int>()
    return elements
        .map {
            if (mapOfOccurrences.getOrPut(it) { 0 } != maxOccurrences) {
                mapOfOccurrences[it] = mapOfOccurrences.get(it)?.inc() ?: 0
                it
            } else {
                null
            }
        }.filterNotNull().toIntArray()
}

fun deleteNth3(elements: IntArray, maxOccurrences: Int): IntArray {
    val mapOfOccurrences = mutableMapOf<Int, Int>()
    return elements
        .asSequence()
        .onEach { mapOfOccurrences[it] = mapOfOccurrences.getOrDefault(it, 0) + 1 }
        .filterNot { mapOfOccurrences[it] == maxOccurrences }
        .toList().toIntArray()
}


fun main() {
    val occurrences = intArrayOf(1, 2, 3, 1, 2, 1, 2, 3)
    println(deleteNth3(occurrences, 2).toList())
}
