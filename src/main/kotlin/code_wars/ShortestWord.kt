package code_wars

fun findShort(s: String): Int {
    return s.split(" ").map { it.length }.minOf { it }
}

fun main() {
    println(findShort2("abcd abc a ab"))
}

fun findShort2(s: String): Int {
    return s.splitToSequence(" ").minOf(String::length)
}