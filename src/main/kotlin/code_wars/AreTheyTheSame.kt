package code_wars

//import java.util.*
import kotlin.math.sqrt

fun comp(a: IntArray?, b: IntArray?): Boolean {
    if (a == null || b == null || a.size != b.size) return false
    return a.map { it * it }.sorted() == b.sorted()
}

fun main() {
    val a1 = intArrayOf(121, 144, 19, 161, 19, 144, 19, 11)
    val a2 = intArrayOf(11*11, 121*121, 144*144, 19*19, 161*161, 19*19, 144*144, 19*19)
    println(comp(a1, a2))
    a1.sort()
    a2.sort()
    println(a1.toList().toString())
    a2.forEach { print("${sqrt(it.toFloat())} ") }
}