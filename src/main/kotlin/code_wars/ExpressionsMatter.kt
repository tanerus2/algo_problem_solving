package code_wars

fun expressionsMatter(a: Int, b: Int, c: Int): Int {
    val values = IntArray(5)
    values[0] = a + b + c
    values[1] = a * b + c
    values[2] = a * (b + c)
    values[3] = (a + b) * c
    values[4] = a * b * c
    return values.maxOf { it }
}

fun main() {
    println("6 == ${expressionsMatter(2, 1, 2)}")
    println("3 == ${expressionsMatter(1, 1, 1)}")
    println("9 == ${expressionsMatter(1, 2, 3)}")
}