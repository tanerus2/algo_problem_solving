package codin_game

// https://www.codingame.com/ide/puzzle/there-is-no-spoon-episode-1

fun main() {
    val nodes = Array(3) { CharArray(3) { ' ' } }

    val row1 = "0.0"
    val row2 = "..."
    val row3 = "0.0"
    val row4 = "0000"

    nodes[0] = row1.toCharArray()
    nodes[1] = row2.toCharArray()
    nodes[2] = row3.toCharArray()
//    nodes[3] = row4.toCharArray()

    nodes.show()
// subList().indexOfFirst() ->
    
    val result = IntArray(6) { -1 }
    val lastRowInd = nodes.size - 1
    val lastColInd = 3 - 1
    for (nodesArray in nodes.withIndex()) {
        for (x in 0 until nodesArray.value.size) {
            val y = nodesArray.index
            if (nodesArray.value[x].isNode()) {
                result[0] = x
                result[1] = y
                // check right neighbor
                for (neighborX in (x + 1)..lastColInd) {
                    if (nodesArray.value[neighborX].isNode()) {
                        result[2] = neighborX
                        result[3] = y
                        break
                    }
                }
                // check bottom neighbor
                for (neighborY in (y + 1)..lastRowInd) {
                    if (nodes[neighborY][x].isNode()) {
                        result[4] = x
                        result[5] = neighborY
                        break
                    }
                }
                println(result.map { it.toString() }.toTypedArray().joinToString(" "))
            }
            result.fill(-1)
        }
    }
}

fun Char.isNode(): Boolean {
    return this == '0'
}

fun Array<CharArray>.show() {
    for (array in this) {
        for (item in array) {
            print("[$item]")
        }
        println()
    }
}